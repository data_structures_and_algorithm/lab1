#include <iostream>
#include <time.h>
#include <istream>

using namespace std;

struct my_list {
	int inf;
	my_list *next;
};

int irand(int left, int right) 
{ return rand() % (right - left + 1) + left; }

my_list *ListConstructer() { return 0; }
my_list *ListConstructer(int n) {
	my_list *first = new my_list, *w;
	first->inf = irand(-5, 5) % 10;
	w = first;
	for (int i = 0; i < n - 1; i++) {
		w->next = new my_list;
		w = w->next;
		w->inf = irand(-5, 5);
	}
	w->next = 0;
	return first;
}
void createList(int n, my_list *&list) {
	list = new my_list;
	my_list *w = list;
	w->inf = irand(-5, 5);
	for (int i = 0; i < n - 1; i++) {
		w->next = new my_list;
		w = w->next;
		w->inf = irand(-5, 5);
	}
	w->next = 0;
}
void writeList(my_list *list) {
	while (list) {
		cout << list->inf << " ";
		list = list->next;
	}
	cout << endl;
}

void addFirst(int n, my_list *&list)
{
	my_list *q = new my_list;
	q->inf = n;
	q->next = list;
	list = q;
}
void add_after_i(int n, int idx, my_list *&list)
{
	if (!list) {
		list = new my_list;
		list->inf = n;
		list->next = 0;
		return;
	}
	my_list *el = list;
	if (idx == -1) {
		addFirst(n, list);
		return;
	}
	for (int i = 0; i < idx; i++) {
		el = el->next;
		if (!el) {
			cout << "Warning! Invalid index\n";
			return;
		}
	}
	my_list *p = new my_list;
	p->inf = n;
	p->next = el->next;
	el->next = p;
}
void insert(int n, int idx, my_list *&list)
{
	my_list *w = new my_list;
	w->next = list;
	my_list *pnt = w;
	for (int i = 0; i < idx; i++) {
		pnt = list;
		list = list->next;
		if (!list) {
			cout << "Warning! Invalid index";
			return;
		}
	}
	pnt->next = new my_list;
	pnt->next->next = list;
	pnt->next->inf = n;
	list = w->next;
	delete w;
}
void push_back(int n, my_list *&list)
{
	my_list *w = new my_list;
	w->next = list;
	my_list *pnt = w;
	while (pnt) {
		list = pnt;
		pnt = pnt->next;
	}
	pnt = new my_list;
	pnt->inf = n;
	pnt->next = 0;
	list->next = pnt;
	list = w->next;
	delete w;
}
//void deleteEl(int idx, my_list *&list)
//{
//	my_list *p, *w, *start_pnt = new my_list;
//	start_pnt->next = list;
//	p = start_pnt;
//	for (int i = 0; i < idx ; i++) p = p->next;
//	w = p->next;
//	p->next = w->next;
//	delete w;
//	list = start_pnt->next;
//	delete start_pnt;
//}
void deleteEl(int idx, my_list *&list)
{
	my_list *w = list, *p = w;
	if (!idx) {
		list = list->next;
		delete w;
		return;
	}
	for (int i = 0; i < idx; i++) {
		p = w;
		w = w->next;
	}
	p->next = w->next;
	delete w;
}
//
void pop_start(my_list *&list)
{
	my_list *w = list;
	list = list->next;
	delete w;
}
void pop_back(my_list *list)
{
	my_list *w = list;
	while (list->next) {
		w = list;
		list = list->next;
	}
	w->next = 0;
	delete list;
}
//
void clear(my_list *&list)
{
	my_list *p = list;
	while (list) {
		list = list->next;
		delete p;
		p = list;
	}
}
int length(my_list *list)
{
	int n = 0;
	while (list) {
		list = list->next;
		n++;
	}
	return n;
}
//�������� ��� ��������� ������, ������ �������� � ������
my_list *getElement(int idx, my_list *list)
{
	for (int i = 0; i < idx; i++) list = list->next;
	return list;
}
//int getIndex(int n, my_list *list)
//{
//	int idx = 0;
//	while (list && (list->inf != n)) {
//		list = list->next;
//		idx++;
//	}
//	if (!list) {
//		return -1;
//	}
//	return idx;
//}
my_list *getIndex(int n, my_list *list) {
	int idx = 0;
	my_list *p = new my_list, *p_pnt = p;
	p->next = 0;
	while (list) {
		if (list->inf == n) {
			p->next =new my_list;
			p = p->next;
			p->inf = idx;
		}
		list = list->next;
		idx++;
	}
	p->next = 0;
	p = p_pnt->next;
	delete p_pnt;
	return p;
}
void copy(my_list *source, my_list *&destination)
{
	destination = new my_list;
	destination->inf = source->inf;
	source = source->next;
	my_list *p = destination;
	while (source) {
		p->next = new my_list;
		p = p->next;
		p->inf = source->inf;
		source = source->next;
	}
	p->next = 0;
}
void fragmentation(my_list *source, my_list *&list1, my_list *&list2)
{
	my_list *p1 = new my_list, *p2 = new my_list;
	my_list *list1_pnt = p1, *list2_pnt = p2;
	while (source) {
		if (source->inf % 2) {
			p2->next = new my_list;
			p2 = p2->next;
			p2->inf = source->inf;
		}
		else {
			p1->next = new my_list;
			p1 = p1->next;
			p1->inf = source->inf;
		}
		source = source->next;
	}
	p1->next = 0;
	p2->next = 0;
	list1 = list1_pnt->next;
	list2 = list2_pnt->next;
	delete list1_pnt;
	delete list2_pnt;
}
void my_sort(my_list *&list)
{
	my_list *list_pnt, *prev_list, *start_pnt;
	my_list *min, *prev_min;
	my_list *sorted_list, *sort_pnt;
	
	list_pnt = list; 
	start_pnt = new my_list;
	start_pnt->next = list;
	prev_list = start_pnt;
	
	sorted_list = new my_list;
	sort_pnt = sorted_list;
	
	while (list_pnt) {

		min = list_pnt; 
		prev_min = prev_list;

		while (list_pnt) {
			if (list_pnt->inf < min->inf) {
				prev_min = prev_list;
				min = list_pnt;
			}
			prev_list = list_pnt;
			list_pnt = list_pnt->next;
		}
		sorted_list->next = new my_list;
		sorted_list = sorted_list->next;
		sorted_list->inf = min->inf;

		prev_min->next = min->next;
		delete min;
		list_pnt = start_pnt->next;
		prev_list = start_pnt;
	}
	sorted_list->next = 0;
	list = sort_pnt->next;
	delete sort_pnt;
	delete start_pnt;
}

void writeMenu()
{
	cout << "0 - Exit" << endl;
	cout << "1 - Generate list" << endl;
	cout << "2 - Write list" << endl;
	cout << "3 - push to start" << endl;
	cout << "4 - push to middle" << endl;
	cout << "5 - push to end" << endl;
	cout << "6 - insert" << endl;
	cout << "7 - delete element" << endl;
	cout << "8 - pop back" << endl;
	cout << "9 - pop start" << endl;
	cout << "10 - clear list" << endl;
	cout << "11 - length" << endl;
	cout << "12 - element by index" << endl;
	cout << "13 - index by element" << endl;
	cout << "14 - copy of list" << endl;
	cout << "15 - list fragmentation" << endl;
	cout << "16 - sort" << endl;
	cout << "20 - write list 2" << endl;
	cout << "21 - clear list 2" << endl;
	cout << "42 - Help" << endl;
}
int main() 
{

	srand((unsigned)time(0));
	my_list *List = 0, *p = 0, *t = 0;
	//cout << List;
	//createList(10, List);
	//writeList(List);


	writeMenu();
	int num = 1;
	while (num) {
		cin >> num;
		switch (num) {
		case 0:break;
		case 1: createList(10, List); break;
		case 2: writeList(List); break;
		case 3: {
			int n; 
			cout << "Write number\n"; 
			cin >> n; 
			addFirst(n, List); 
			break; 
		}
		case 4: {
			int n,idx; 
			cout << "Write number\n"; 
			cin >> n;
			cout << "Write position\n";
			cin >> idx;
			add_after_i(n, idx, List); 
			break; 
		}
		case 5: {
			int n;
			cout << "Write number\n";
			cin >> n;
			push_back(n, List);
			break;
		}
		case 6: {
			int n, idx;
			cout << "Write number\n";
			cin >> n;
			cout << "Write position\n";
			cin >> idx;
			insert(n, idx, List);
			break;
		}
		case 7: {
			int idx;
			cout << "Write position\n";
			cin >> idx;
			deleteEl(idx, List);
			break;
		}
		case 8: pop_back(List); break;
		case 9: pop_start(List); break;
		case 10: clear(List); break;
		case 11: cout << length(List) << endl; break;
		case 12: {
			int idx;
			cout << "Write position\n";
			cin >> idx;
			my_list *a = getElement(idx, List);
			cout << a->inf << endl;
			break;
		}
		case 13: {
			int n;
			cout << "Write element\n";
			cin >> n;
			writeList(getIndex(n, List));
			break;
		}
		case 14: copy(List, p); break;
		case 15: fragmentation(List, p, t); break;
		case 16: my_sort(List); break;
		case 20: writeList(p); break;
		case 21: clear(p); break;
		case 22: writeList(t); break;
		case 23: clear(t); break;
		case 42: writeMenu(); break;
		default: cout << "Error! Invalid value!\n";
		}
	}


	system("pause");
	return 0;
}